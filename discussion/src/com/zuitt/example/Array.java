package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class Array {
    // [Section] Java Collections
    // are a single unit of objects.
    // Useful for manipulating relevant pieces of data that can be used in different situations, commonly used in loops.
    public static void main(String[] args){
        // This is where we add our codebase
        // [Section] Array
        // In Java, arrays are containers of values of the same type given a predefined amount/number of values.
        // Java arrays are more rigid, once the size and data type are defined, they can no longer be changed.

        // Syntax for Array Declaration:
            // dataType[] identifier = new dataType[numOfElements];
            // "[]" indicates that the data type should be able to hold multiple values.
            // "new" keyword is used for non-primitive data types to tell Java to create the said variable.
            // "numOfElements" will tell how many elements does our array can hold.
        // Array Declaration:
        //if we are going to declare an array of int[], the default value of the elements will all be 0.
        //if array of String, the default value of the elements will be null;

        int[] intArray = new int[5];

        // This will just print out the memory address of the Array.
        // System.out.println(intArray);

        // To initialize the value of our elements inside the array, we are going to use the index:

        intArray[0] = 200;
        intArray[1] = 645;
        intArray[2] = 154;
        intArray[3] = 824;

        // To print out the intArray, we need to import the Array class and use the .toString() method.
        // This method will convert the array as a string in the terminal.
        System.out.println("Array Examples: ");
        System.out.println(Arrays.toString(intArray));

        // Declaration and Initialization of an Array
        // Syntax:
            // dataType[] identifier = {elementA, elementB, ...};
            // the compiler automatically specifies the size by counting the number of elements during the initialization.

        String[] names = {"Aaron", "Cyrus", "Cooper", "Lance"};

        System.out.println(Arrays.toString(names));

        // Sample Java Array Methods:
        // Sort Method:
        Arrays.sort(intArray);

        System.out.println("Order of int numbers after sort: " + Arrays.toString(intArray));

        // Multidimensional Array
        // A two-dimensional array, can be described by two lengths nested within each other, like a matrix.
            //First length is row, second length is the column, arrayName[][]
        // [['as', 'bs', 'ds'], ['as', 'bs', 'ds']]
        String[][] classroom = new String[3][3];
        // First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        // Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jun Jun";
        classroom[1][2] = "Robert";

        // Third Row
        classroom[2][0] = "Dane";
        classroom[2][1] = "Bailu";
        classroom[2][2] = "Gerard";

        // This is only applicable to those two-dimensional
        System.out.println(Arrays.deepToString(classroom));

        // In Java, the size of the array cannot be modified. If there is a need to add or remove elements, you need to create a new array.

        // [Section] ArrayList
            // Resizable arrays, wherein elements can be added or removed whenever it is needed.
            // Syntax:
                // ArrayList<T> identifier = new ArrayList<T>();
                // "<T>" is used to specify that the list can only have one type of object in a collection.
                // ArrayList cannot hold primitive data types, "java wrapper classes" provide a way to use this types as object.
                // In short, Object version of primitive data type with methods.

        // Declaration of an ArrayList:
            // Example of usage of primitive data type as a generic in the ArrayList.
            // ArrayList<int> numbers = new Arraylist<int>();

        // Usage of Integer
        ArrayList<Integer> numbers = new ArrayList<>();

        System.out.println("Array List Examples: ");

        // Add elements
        // arrayName.add(element);
        numbers.add(5);
        System.out.println(numbers);

        // Access Element
        // arrayListName.get(index);
        System.out.println(numbers.get(0));

        // Declaration with Initialization:
        ArrayList<String> students = new ArrayList<>(Arrays.asList("Milky", "Ven"));
        System.out.println(students);

        // Add Element/s on the ArrayList students:
        students.add("John");
        System.out.println(students);

        // Access the Elements
        System.out.println(students.get(2));

        // Updating an element
        // arrayListName.set(index, updatedValue/Element);
        students.set(2, "Juan");
        System.out.println(students);

        // Remove a specific element
        // arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        // Getting the arrayList size:
        // arrayListName.size();
        System.out.println("Length of Student arrayList: " + students.size());

        // Removing all the elements:
        // arrayListName.clear();
        students.clear();
        System.out.println(students);

        // More Resources of ArrayList: https://www.programiz.com/java-programming/library/arraylist

        /*
         [Section] Hashmaps
         Most objects in Java are defined and are instantiated of Classes that contain a proper set of properties and methods.
         There might be use cases where it is not appropriate, or you may simply want a collection of data in key-value pairs.
         In Java, "Keys" are also referred as "fields" wherein the values can be accessed through fields.
         Syntax:
            HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();
        */

        // Declaration of HashMap:
        HashMap<String, String> jobPosition = new HashMap<>();

        System.out.println(jobPosition);

        // Methods in Hashmap:
        // Add key-value pair
        // HashMapName.put(fieldName, fieldValue);
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
        System.out.println(jobPosition);

        // The last one will be overridden whenever a same key is used.
        jobPosition.put("Student", "Zeke");
        System.out.println(jobPosition);

        // Accessing element - we use the field name because they are unique, hence key-pair.
        // hashMapName.get("fieldName");
        System.out.println(jobPosition.get("Student"));

        // Updating Values
        // HashMapName.replace("fieldNameToChange", "newValue");
        jobPosition.replace("Student", "Zeke Kai");
        System.out.println(jobPosition);

        // Remove an element
        // HashMapName.remove("key");
        // jobPosition.remove("Dreamer");
        // System.out.println(jobPosition);

        // Retrieve HashMap keys
        // HashMapName.keySet();
        System.out.println(jobPosition.keySet());

        // Retrieve the value from the HashMap
        // HashMapName.values();
        System.out.println(jobPosition.values());

        // Remove all the key-value pairs
        // HashMapName.clear();
        jobPosition.clear();
        System.out.println(jobPosition);

        // Declaration of HashMap with Initialization
        HashMap<String, String> jobPosition2 = new HashMap<>(){
            {
                put("Teacher", "John");
                put("Artists", "Jane");
            }
        };

        System.out.println(jobPosition2);

    }
}
