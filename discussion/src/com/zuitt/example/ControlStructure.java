package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
        // [Section] Java Operators
        // Arithmetics: +, -, /, *, %;
        // Comparison: <, >, >=, <=, ==, !=;
        // Logical: &&, ||, !;
        // Re/assignment operator: =, +=, -=, *=, /=;
        // Increment and decrement: i++, ++i, i--, --i;

        // [Section] Selection Control Structure:
        // Statement allows us to manipulate the flow of the code depending on the evaluation of the condition;
        // Syntax:
        /*
            if (condition){
                //statement
            }
            else{
                //statement
            }
        */
        Scanner numScanner = new Scanner(System.in);
        System.out.print("Enter a whole number to see if it's divisible by 5: ");
        int num1 = numScanner.nextInt();
        if (num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5!");
        } else {
            System.out.println(num1 + " is not divisible by 5!");
        }

        // [Section] Short Circuiting
        // A technique applicable only to AND & OR operators wherein if statement/s or other controls exits early by ensuring safety operation or for efficiency
        // if (condition1 || condition2 || condition3 || condition4)

        // [Section] Ternary Operator
        System.out.print("Enter a whole number if it's greater than 0 or not: ");
        int num2 = numScanner.nextInt();
        Boolean result = (num2 > 0) ? true : false;

        System.out.println(num2 + " being greater than 0 is " + result + "!");

        // [Section] Switch Statement:
        // Are used to control the flow structures that allow one code block out of many other code blocks;
        System.out.print("Enter a whole number from 1-4: ");
        int directionValue = numScanner.nextInt();

        switch(directionValue){
            case 1:
                System.out.println("North!");
                break;
            case 2:
                System.out.println("South!");
                break;
            case 3:
                System.out.println("East!");
                break;
            case 4:
                System.out.println("West!");
                break;
            default:
                System.out.println("Invalid. Number is not between 1-4.");

        }

    }
}
